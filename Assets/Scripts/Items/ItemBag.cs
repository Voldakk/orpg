﻿using UnityEngine;

[System.Serializable]
[CreateAssetMenu(fileName = "New Bag", menuName = "Items/Bag")]
public class ItemBag : Item
{
    public int size;

    public override void OnInventoryActivated()
    {
        base.OnInventoryActivated();

        PlayerInventory inventory = PlayerInventory.instance;

        for (int i = 0; i < inventory.maxBags; i++)
        {
            if (!inventory.HasBag(i))
            {
                inventory.SwitchToBag(i, inventoryBagIndex , inventoryItemIndex);
                FindObjectOfType<BagsUI>().ToggleBag(i, true);
                break;
            }
        }
    }
}
