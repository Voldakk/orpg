﻿using UnityEngine;

[CreateAssetMenu(fileName = "New Weapon", menuName = "Items/Weapon")]
public class EquipmentWeapon : ItemEquipment
{
    public Range damage;
    public float attackSpeed;
    public bool twoHanded;
}