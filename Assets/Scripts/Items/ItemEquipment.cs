﻿using UnityEngine;

public enum EquipmentSlot { Head, Chest, Legs, Feet, Hands, MainHand, OffHand };
public enum EquipmentMeshRegion { Neck, Chest, Stomach, Shoulders, UpperArms, Elbows, LowerArms, Hands, Hips, Thighs, Knees, Shins, Ankles, feet};

public class ItemEquipment : Item
{
    [Header("Mesh")]
    public SkinnedMeshRenderer prefab;
    [HideInInspector]
    public SkinnedMeshRenderer mesh;

    [Header("Slots")]
    public EquipmentSlot equipmentSlot;
    public EquipmentMeshRegion[] meshRegions;

    [Header("Stats")]
    public int minLevel = 1;
    public int stamina;
    public int strength;
    public int intellect;
    public int agility;
    public int spirit;

    public override void OnInventoryActivated()
    {
        base.OnInventoryActivated();

        PlayerEquipment.instance.EquipItemFromInventory(this, equipmentSlot);
    }
}
