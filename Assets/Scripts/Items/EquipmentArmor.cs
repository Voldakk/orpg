﻿using UnityEngine;

[CreateAssetMenu(fileName = "New Armor", menuName = "Items/Armor")]
public class EquipmentArmor : ItemEquipment
{
    public int armor;
}