﻿using UnityEngine;

[System.Serializable]
[CreateAssetMenu(fileName = "New Item", menuName = "Items/Item")]
public class Item : ScriptableObject
{
    new public string name;
    public Sprite icon;
    public int stackSize = 1;
    public int count = 1;

    public virtual bool Equals(Item other)
    {
        return name == other.name;
    }

    public virtual void OnInventoryActivated()
    {
        Debug.Log("Activated " + name);
    }

    [HideInInspector]
    public bool inInventory = false;
    [HideInInspector]
    public int inventoryBagIndex;
    [HideInInspector]
    public int inventoryItemIndex;

    public bool RemoveFromInventory()
    {
        return PlayerInventory.instance.Remove(this);
    }
}