﻿using UnityEngine;

[System.Serializable]
public struct Range
{
    public int min, max;

    /// <summary>
    /// A range of two values
    /// </summary>
    /// <param name="min">The min value</param>
    /// <param name="max">The max value</param>
    public Range(int min, int max)
    {
        this.min = min;
        this.max = max;
    }

    /// <summary>
    ///  A range of two values
    /// </summary>
    /// <param name="value">The min and max value</param>
    public Range(int value)
    {
        min = value;
        max = value;
    }

    public static Range operator +(Range a, Range b)
    {
        return new Range(a.min + b.min, a.max + b.max);
    }

    public static Range operator +(Range a, int b)
    {
        return new Range(a.min + b, a.max + b);
    }

    /// <summary>
    /// Calculates the average of the min and max value of the range
    /// </summary>
    /// <returns>The average as float</returns>
    public float Average()
    {
        return (min + max) / 2.0f;
    }

    /// <summary>
    /// Returns the range as a formated string
    /// </summary>
    /// <returns>String</returns>
    public override string ToString()
    {
        return string.Format("{0} - {1}", min, max);
    }

    /// <summary>
    /// Returns a random value in the range
    /// </summary>
    /// <returns>A random int</returns>
    public int Random()
    {
        return UnityEngine.Random.Range(min, max);
    }
}

[System.Serializable]
public struct LevelRange
{
    public AnimationCurve min, max;

    /// <summary>
    /// A range of two curves to define a range for each character level
    /// </summary>
    /// <param name="min">The min curve</param>
    /// <param name="max">The max curve</param>
    public LevelRange(AnimationCurve min, AnimationCurve max)
    {
        this.min = min;
        this.max = max;
    }

    /// <summary>
    /// Returns the range for a given level
    /// </summary>
    /// <param name="level">The charachers level</param>
    /// <returns>The range for the given level</returns>
    public Range GetRange(int level)
    {
        return new Range(Mathf.RoundToInt(min.Evaluate(level)), Mathf.RoundToInt(max.Evaluate(level)));
    }
}