﻿using UnityEngine;

[CreateAssetMenu(fileName = "New interactable", menuName = "Data/InteractableInfo")]
public class InteractableInfo : ScriptableObject
{
    new public string name = "New interactable";
    public Sprite icon;
}
