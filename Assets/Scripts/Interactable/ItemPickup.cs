﻿using UnityEngine;
using System.Collections.Generic;

public class ItemPickup : Interactable
{
    // Items to put in the inventory if picked up
    public List<Item> items;

    // When the player interacts with the item
    public override void Interact()
    {
        base.Interact();
        interactable = false;

        PickUp();
    }

    // Pick up the item
    void PickUp()
    {
        LootPanel.instance.SetLoot(items, OnLootComplete, OnLootAborted, LootType.Container);
    }

    public void OnLootComplete()
    {
        isFocus = false;
    }
    public void OnLootAborted(List<Item> items)
    {
        this.items = items;
        interactable = true;
    }
}