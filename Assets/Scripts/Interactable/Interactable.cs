﻿using UnityEngine;

public class Interactable : MonoBehaviour
{
    public InteractableInfo info;

    // The radius to the player in which the object is interactable
    public float radius = 3f;

    // Is this interactable currently being focused?
    protected bool isFocus = false;       
    
    // Is the object currently interactable
    protected bool interactable = true;     

    // Refference to the player controller
    PlayerController player;


    // Called when the object starts being focused
    public void OnFocused(PlayerController player)
    {
        isFocus = true;
        this.player = player;
    }

    // Called when the object is no longer focused
    public void OnDefocused()
    {
        isFocus = false;
        player = null;
    }

    // Called when the object is activated by the player
    public void OnActivate()
    {
        // If currently being focused and the object is interactable
        if (isFocus && interactable)    
        {
            // If the player is close enough
            if (Vector3.Distance(player.transform.position, transform.position) <= radius)
            {
                Interact();
            }
        }
    }

    // This method is meant to be overwritten
    public virtual void Interact()
    {

    }

    void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.yellow;
        Gizmos.DrawWireSphere(transform.position, radius);
    }
}