﻿using UnityEngine;
using UnityEngine.UI;
public class LootItemPanel : MonoBehaviour
{
    public Image icon;
    public TMPro.TextMeshProUGUI count;
    public TMPro.TextMeshProUGUI itemName;

    public int index;

    public void SetItem(Item item, int index)
    {
        gameObject.SetActive(true);

        this.index = index;
        icon.enabled = true;
        icon.sprite = item.icon;
        itemName.text = item.name;
        if (item.stackSize > 1)
            count.text = item.count.ToString();
        else
            count.text = "";
    }

    public void OnClicked()
    {
        LootPanel.instance.OnItemClicked(index);
    }
}
