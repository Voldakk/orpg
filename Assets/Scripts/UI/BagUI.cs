﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public class BagUI : MonoBehaviour
{
    public  List<ItemPanel> items = new List<ItemPanel>();

    public RectTransform rt;

    public GridLayoutGroup grid;
    public RectTransform gridRt;

    public int index;
    public BagsUI bagsUI;

    public TMPro.TextMeshProUGUI bagName;
    public Image bagIcon;

    public void SetBag (ItemBag newBag)
    {
        if (newBag != null)
        {
            bagName.text = newBag.name;
            bagIcon.sprite = newBag.icon;
        }
    }
    public void OnCloseButtonClicked()
    {
        bagsUI.OnBagCloseButtonClicked(index);
    }
}
