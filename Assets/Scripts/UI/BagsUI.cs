﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

[RequireComponent(typeof(RectTransform))]
public class BagsUI : DragAndDropContainer
{
    PlayerInventory inventory;
    PlayerEquipment equipment;

    public GameObject BagCollumnPrefab;
    public GameObject BagPrefab;
    public GameObject ItemPanelPrefab;

    private List<RectTransform> collumns;
    private List<BagUI> bags;

    public int bagHeight;

    public List<ItemPanel> bagPanels;

    /// <summary>
    /// Use this for initialization
    /// </summary>
    protected override void Initialize()
    {
        base.Initialize();

        // Get the player inventory
        inventory = PlayerInventory.instance;
        // Subscribe to the onItemChangedCallback
        inventory.onItemChangedCallback += OnInventoryChanged;

        // Clear out any test elements
        for (int i = 0; i < transform.childCount; i++)
        {
            Destroy(transform.GetChild(i).gameObject);
        }

        // The collum and bags lists
        collumns = new List<RectTransform>();
        bags = new List<BagUI>();

        // Add the initial collum
        collumns.Add(Instantiate(BagCollumnPrefab, transform).GetComponent<RectTransform>());

        // Add all the bags
        for (int i = 0; i < inventory.maxBags; i++)
        {
            bags.Add(Instantiate(BagPrefab, collumns[0]).GetComponent<BagUI>());
        }
        bags.Reverse();

        // Set the bag properties
        for (int i = 0; i < inventory.maxBags; i++)
        {
            bags[i].index = i;
            bags[i].bagsUI = this;
            bags[i].SetBag(inventory.bags[i]);
            bags[i].gameObject.SetActive(false);
            bagPanels[i].SetObject(inventory.bags[i]);
            bagPanels[i].SetIndeces(containerIndex, new int[] { -1, i });
        }
        
        BuildBags();
    }

    /// <summary>
    /// Whenever the players inventory has changed
    /// </summary>
    void OnInventoryChanged()
    {
        BuildBags();
    }

    /// <summary>
    /// Builds the bags UI elements
    /// Instantiates new item panels if needed and assigns items to the item panels
    /// Hides any inactive item panels
    /// Adjusts the bags height in the UI
    /// </summary>
    void BuildBags()
    {
        for (int i = 0; i < inventory.maxBags; i++)
        {
            bags[i].SetBag(inventory.bags[i]);
            bagPanels[i].SetObject(inventory.bags[i]);
        }

        for (int i = 0; i < inventory.maxBags; i++)
        {
            Inventory current = inventory.inventories[i];

            if (current != null && current.bag != null)
            {
                int itemCount = bags[i].items.Count;
                for (int n = itemCount; n < current.bag.size; n++)
                {
                    bags[i].items.Add(Instantiate(ItemPanelPrefab, bags[i].gridRt).GetComponent<ItemPanel>());
                    bags[i].items[n].SetIndeces(containerIndex, new int[] { bags[i].index, n });
                }

                for (int n = 0; n < current.bag.size; n++)
                {
                    if(!bags[i].items[n].gameObject.activeSelf)
                        bags[i].items[n].gameObject.SetActive(true);

                    bags[i].items[n].SetObject(current.items[n]);
                }
                for (int n = current.bag.size; n < bags[i].items.Count; n++)
                {
                    bags[i].items[n].gameObject.SetActive(false);
                }

                bags[i].gridRt.sizeDelta = new Vector2(bags[i].grid.cellSize.x * bags[i].grid.constraintCount, GridHeight(i));
                bags[i].rt.sizeDelta = new Vector2(bags[i].rt.sizeDelta.x, BagHeight(i));
            }
        }
    }

    /// <summary>
    /// Called every frame by the engine
    /// </summary>
    public void Update()
    {
        if(Input.GetButtonDown("Bags"))
        {
            bool anyOpen = false;
            for (int i = 0; i < inventory.maxBags; i++)
            {
                if (inventory.bags[i] != null && bags[i].gameObject.activeSelf)
                    anyOpen = true;
            }
            for (int i = 0; i < inventory.maxBags; i++)
            {
                if (inventory.bags[i] != null)
                {
                    ToggleBag(i, !anyOpen);
                }
            }
        }
    }

    /// <summary>
    /// When the close button on any bag is pressed
    /// </summary>
    /// <param name="index">The bags index</param>
    public void OnBagCloseButtonClicked(int index)
    {
        bags[index].gameObject.SetActive(false);
    }

    /// <summary>
    /// Toggles a bags on or off
    /// </summary>
    /// <param name="bagIndex">The bags index</param>
    public void ToggleBag(int bagIndex)
    {
        ToggleBag(bagIndex, !bags[bagIndex].gameObject.activeSelf);
    }

    /// <summary>
    /// Toggles a bags on or off
    /// </summary>
    /// <param name="bagIndex">The bags index</param>
    /// <param name="active">The desired active state (on / off)</param>
    public void ToggleBag(int bagIndex, bool active)
    {
        
        bags[bagIndex].gameObject.SetActive(active);

        UpdateBags();
    }

    /// <summary>
    /// Updates the bags
    /// Moves the bags between collums to respect the bag panels' height limit
    /// Instantiates more collumns if needed
    /// </summary>
    public void UpdateBags()
    {
        int collumn = 0;
        int height = 0;
        for (int i = 0; i < inventory.maxBags; i++)
        {
            if (inventory.HasBag(i) && bags[i].gameObject.activeSelf)
            {
                height += BagHeight(i);
                if (height > collumns[collumn].sizeDelta.y)
                {
                    collumn++;
                    if (collumn == collumns.Count)
                    {
                        collumns.Add(Instantiate(BagCollumnPrefab, transform).GetComponent<RectTransform>());
                        collumns[collumn].transform.SetAsFirstSibling();
                    }

                    bags[i].transform.SetParent(collumns[collumn]);
                    bags[i].transform.SetAsFirstSibling();
                    height = BagHeight(i);
                }
                else
                {
                    bags[i].transform.SetParent(collumns[collumn]);
                    bags[i].transform.SetAsFirstSibling();
                }
            }
        }
    }

    /// <summary>
    /// Calculates the height of the item panel grid based of the bags inventory size
    /// </summary>
    /// <param name="bagIndex">The bag index</param>
    /// <returns>The height</returns>
    public int GridHeight(int bagIndex)
    {
        return (int)bags[bagIndex].grid.cellSize.y * Mathf.CeilToInt((float)inventory.inventories[bagIndex].bag.size / bags[bagIndex].grid.constraintCount);
    }

    /// <summary>
    /// Calculates the height of the bag based of the bags inventory size
    /// The GridHeight + additional UI elements
    /// </summary>
    /// <param name="bagIndex">The bag index</param>
    /// <returns>The height</returns>
    public int BagHeight(int bagIndex)
    {
        return GridHeight(bagIndex) + bagHeight;
    }

    /// <summary>
    /// Return the object in the given indices
    /// </summary>
    /// <param name="indices">The indeces in which the object is stored</param>
    /// <returns>The object in the given indices</returns>
    public override object GetObject(int[] indices, bool isFromContainer)
    {
        if (indices.Length != 2)
            return null;

        if (indices[0] == -1)
            return inventory.bags[indices[1]];  // Get a bag
        else
            return inventory.GetItem(indices[0], indices[1]); // Gat an item from a bag
    }

    /// <summary>
    /// Remove the object in the given indices
    /// </summary>
    /// <param name="indices">The indeces in which the object is stored</param>
    public override void RemoveObject(int[] indices)
    {
        if (indices.Length != 2)
            return;

        if (indices[0] == -1)
            return; //inventory.RemoveBag(indices[0]); // Remove a bag
        else
            inventory.Remove(indices[0], indices[1]); // Remove an item from a bag
    }

    /// <summary>
    /// Try to recieve an object form another container
    /// </summary>
    /// <param name="o">The object to recieve</param>
    /// <param name="indices">The indeces in which the object is stored</param>
    /// <returns>If the container sucsessfully recieved the object</returns>
    public override bool RecieveObject(object o, int[] indices)
    {
        if (indices.Length != 2 || !(o is Item))
            return false;

        return inventory.Add(o as Item, indices[0], indices[1]);
    }

    /// <summary>
    /// Switch two objects in the same container
    /// </summary>
    /// <param name="fromIndices">The indeces in which the first object is stored</param>
    /// <param name="toIndices">The indeces in which the second object is stored</param>
    public override void InternalSwitch(int[] fromIndices, int[] toIndices)
    {
        if (toIndices[0] == -1 && fromIndices[0] == -1) // Switch two bags
        {
            inventory.SwitchBag(fromIndices[1], toIndices[1]);

            bool active = bags[fromIndices[1]].gameObject.activeSelf;
            ToggleBag(fromIndices[1], bags[toIndices[1]].gameObject.activeSelf);
            ToggleBag(toIndices[1], active);
        }
        else if (toIndices[0] == -1) // Equip a bag
        {
            inventory.SwitchToBag(toIndices[1], fromIndices[0], fromIndices[1]);

            // Open the new bag
            ToggleBag(toIndices[1], inventory.HasBag(toIndices[1]));
        }
        else if (fromIndices[0] == -1) // Unequip a bag
        {
            inventory.SwitchFromBag(fromIndices[1], toIndices[0], toIndices[1]);

            // Open the bag if a new one is equiped
            ToggleBag(fromIndices[1], inventory.HasBag(fromIndices[1]));
        }
        else
        {
            inventory.SwitchItem(fromIndices[0], fromIndices[1], toIndices[0], toIndices[1]);
        }
    }

    /// <summary>
    /// When an object is clicked
    /// </summary>
    /// <param name="button">The mouse button that was pressed</param>
    /// <param name="indices">The indeces in which the object is stored</param>
    public override void OnObjectMouseDown(PointerEventData.InputButton button, int[] indices)
    {
        if (indices.Length != 2)
            return;

        if (indices[0] == -1)
        {
            ToggleBag(indices[1]);
        }
        else if(inventory.inventories[indices[0]].items[indices[1]] != null)
        {
            inventory.inventories[indices[0]].items[indices[1]].OnInventoryActivated();
        }
    }
}