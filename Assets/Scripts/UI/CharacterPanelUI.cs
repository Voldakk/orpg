﻿using UnityEngine;
using UnityEngine.UI;

public class CharacterPanelUI : MonoBehaviour
{
    [SerializeField]
    private Character character;

    public Image characterPotrait;
    public TMPro.TextMeshProUGUI characterName;

    public Slider healthSlider;
    public TMPro.TextMeshProUGUI healthText;
    public Slider manaSlider;
    public TMPro.TextMeshProUGUI manaText;

    void Awake()
    {
        if(character != null)
        {
            character.onStatsChangedCallback += OnCharacterStatusChanged;
        }
    }

    public void SetCharacter(Character character)
    {
        if (this.character == character)
            return;

        if(this.character != null)
            this.character.onStatsChangedCallback -= OnCharacterStatusChanged;

        this.character = character;

        if(character != null)
            character.onStatsChangedCallback += OnCharacterStatusChanged;

        /// Set character potrait

        OnCharacterStatusChanged();
    }

    private void OnCharacterStatusChanged()
    {
        if (character == null)
            return;

        characterPotrait.sprite = character.info.icon;
        characterName.text = character.info.name;

        healthSlider.maxValue = character.GetMaxHealth();
        healthSlider.value = character.GetCurrentHealth();
        healthText.text = string.Format("{0} / {1}", character.GetCurrentHealth(), character.GetMaxHealth());

        manaSlider.maxValue = character.GetMaxMana();
        manaSlider.value = character.GetCurrentMana();
        manaText.text = string.Format("{0} / {1}", character.GetCurrentMana(), character.GetMaxMana());
    }
}