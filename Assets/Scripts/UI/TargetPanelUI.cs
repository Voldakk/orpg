﻿using UnityEngine;

public class TargetPanelUI : CharacterPanelUI
{
    [SerializeField] Character owner;

    private void Start()
    {
        if(owner != null)
        {
            owner.onTargetChangedCallback += OnTargetChanged;
        }

        gameObject.SetActive(false);
    }

    private void OnTargetChanged()
    {
        if (owner.target != null && owner.target is Character)
        {
            Character target = owner.target as Character;

            SetCharacter(target);

            gameObject.SetActive(true);
        }
        else
        {
            gameObject.SetActive(false);
        }
    }
}