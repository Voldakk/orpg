﻿using UnityEngine.EventSystems;

public class EquipmentUI : DragAndDropContainer
{
    PlayerEquipment equipment;

    public ItemPanel[] equipmentPanels;

    /// <summary>
    /// Use this for initialization
    /// </summary>
    protected override void Initialize()
    {
        base.Initialize();

        // Get the player equipment
        equipment = PlayerEquipment.instance;

        // Subscribe to the onEquipmentChangedCallback
        equipment.onEquipmentChangedCallback += OnEquipmentChanged;

        for (int i = 0; i < equipmentPanels.Length; i++)
        {
            equipmentPanels[i].SetIndeces(containerIndex, new int[] { i });
        }
    }

    /// <summary>
    /// Whenever the player's equipment has changed
    /// </summary>
    private void OnEquipmentChanged(ItemEquipment from, ItemEquipment to)
    {
        if(from != null)
            equipmentPanels[(int)from.equipmentSlot].SetObject(null);
        if(to != null)
            equipmentPanels[(int)to.equipmentSlot].SetObject(to);
    }

    /// <summary>
    /// Return the object in the given indices
    /// </summary>
    /// <param name="indices">The indeces in which the object is stored</param>
    /// <returns>The object in the given indices</returns>
    public override object GetObject(int[] indices, bool isFromContainer)
    {
        if (indices.Length == 1)
            return equipment.GetEquipment(indices[0]);
        else
            return null;
    }

    /// <summary>
    /// Remove the object in the given indices
    /// </summary>
    /// <param name="indices">The indeces in which the object is stored</param>
    public override void RemoveObject(int[] indices)
    {
        if (indices.Length == 1)
            equipment.UnequipItem(indices[0]);
    }

    /// <summary>
    /// Try to recieve an object form another container
    /// </summary>
    /// <param name="o">The object to recieve</param>
    /// <param name="indices">The indeces in which the object is stored</param>
    /// <returns>If the container sucsessfully recieved the object</returns>
    public override bool RecieveObject(object o, int[] indices)
    {
        if (!(o is ItemEquipment) || indices.Length != 1)
            return false;

        return equipment.EquipItem(o as ItemEquipment, indices[0]);
    }

    /// <summary>
    /// When an object is clicked
    /// </summary>
    /// <param name="button">The mouse button that was pressed</param>
    /// <param name="indices">The indeces in which the object is stored</param>
    public override void OnObjectMouseDown(PointerEventData.InputButton button, int[] indices)
    {
        if (indices.Length != 1)
            return;

        // On right click
        if(button == PointerEventData.InputButton.Right)
            // Unequip the item
            equipment.UnequipItemToInventory(indices[0]);
    }
}