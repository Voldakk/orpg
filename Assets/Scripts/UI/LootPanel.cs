﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public enum LootType { Kill, Container, Mining }

public class LootPanel : MonoBehaviour
{
    public static LootPanel instance;

    void Awake()
    {
        if (instance == null)
            instance = this;
        else
            Debug.LogError("More than one loot panel!");
    }

    void Start()
    {
        for (int i = 0; i < content.childCount; i++)
        {
            Destroy(content.GetChild(i).gameObject);
        }

        gameObject.SetActive(false);
    }

    public TMPro.TextMeshProUGUI title;

    public Image icon;
    public List<Sprite> icons;

    public Transform content;
    public ScrollRect sr;
    public GameObject lootItemPanelPrefab;

    List<Item> items;
    List<LootItemPanel> panels = new List<LootItemPanel>();

    // Callback for when all loot is taken
    public delegate void OnLootCopmpleted();
    private OnLootCopmpleted onLootCompleted;

    // Callback for whan thew loot window is closed with loot remaining
    public delegate void OnLootAborted(List<Item> items);
    private OnLootAborted onLootAborted;

    // Opens the loot window with the provided items
    public void SetLoot(List<Item> itemList, OnLootCopmpleted completed, OnLootAborted aborted, LootType type = LootType.Kill, string title = "Items")
    {
        gameObject.SetActive(true);

        onLootCompleted = completed;
        onLootAborted = aborted;

        items = itemList;
        icon.sprite = icons[(int)type];
        this.title.text = title;

        for (int i = panels.Count; i < items.Count; i++)
        {
            panels.Add(Instantiate(lootItemPanelPrefab, content).GetComponent<LootItemPanel>());
        }
        for (int i = 0; i < items.Count; i++)
        {
            panels[i].SetItem(items[i], i);
            panels[i].gameObject.SetActive(true);
        }
        for (int i = items.Count; i < panels.Count; i++)
        {
            panels[i].gameObject.SetActive(false);
        }

        // Force the ScrollRect to update the viewport size as it sometimes collapses to width=0  
        sr.viewport.sizeDelta = Vector2.one;
    }

    // When an iten in the loot window is clicked
    public void OnItemClicked(int index)
    {
        if(PlayerInventory.instance.Add(Instantiate(items[index])))
        {
            items[index] = null;
            panels[index].gameObject.SetActive(false);

            if(Empty())
            {
                Close();
            }
        }
    }

    // If all the loot is taken
    bool Empty()
    {
        for (int i = 0; i < items.Count; i++)
        {
            if (items[i] != null)
                return false;
        }
        return true;
    }

    // Close the window
    public void Close()
    {
        gameObject.SetActive(false);

        if(Empty())
        {
            onLootCompleted();
        }
        else
        {
            for (int i = items.Count-1; i >= 0; i--)
            {
                if (items[i] == null)
                    items.RemoveAt(i);
            }
            onLootAborted(items);
        }
    }
}