﻿using UnityEngine;
using UnityEngine.UI;

public class HoverPanel : MonoBehaviour
{
    [SerializeField] PlayerController playerController;
    [SerializeField] Image interactableIcon;
    [SerializeField] TMPro.TextMeshProUGUI interactableName;
    [SerializeField] Slider characterHealthBar;

    Interactable interactable;
    Character character;

    private void Start()
    {
        if (playerController != null)
        {
            playerController.onHoverChangedCallback += OnHoverChanged;
        }

        gameObject.SetActive(false);
        characterHealthBar.gameObject.SetActive(false);
    }

    private void OnHoverChanged()
    {
        if (playerController.hover != null)
        {
            SetInteractable(playerController.hover);

            gameObject.SetActive(true);
        }
        else
        {
            gameObject.SetActive(false);
        }
    }

    public void SetInteractable(Interactable interactable)
    {
        if (this.interactable == interactable)
            return;

        if (this.character != null)
        {
            character.onStatsChangedCallback -= OnCharacterStatusChanged;
            character = null;
            characterHealthBar.gameObject.SetActive(false);
        }

        this.interactable = interactable;

        gameObject.SetActive(interactable != null);

        if (interactable != null)
        {
            interactableName.text = interactable.info.name;
            interactableIcon.sprite = interactable.info.icon;

            if(interactable is Character)
            {
                character = interactable as Character;
                character.onStatsChangedCallback += OnCharacterStatusChanged;
                OnCharacterStatusChanged();

                characterHealthBar.gameObject.SetActive(true);
            }
        }
    }

    private void OnCharacterStatusChanged()
    {
        characterHealthBar.maxValue = character.GetMaxHealth();
        characterHealthBar.value = character.GetCurrentHealth();
    }
}
