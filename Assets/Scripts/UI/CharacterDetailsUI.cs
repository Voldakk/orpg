﻿using UnityEngine;

public class CharacterDetailsUI : MonoBehaviour
{
    public TMPro.TextMeshProUGUI maxHealth;
    public TMPro.TextMeshProUGUI healthRegen;
    public TMPro.TextMeshProUGUI maxMana;
    public TMPro.TextMeshProUGUI manaRegen;
    public TMPro.TextMeshProUGUI armor;

    public TMPro.TextMeshProUGUI stamina;
    public TMPro.TextMeshProUGUI strength;
    public TMPro.TextMeshProUGUI intellect;
    public TMPro.TextMeshProUGUI agility;
    public TMPro.TextMeshProUGUI spirit;

    public TMPro.TextMeshProUGUI mainhandDamage;
    public TMPro.TextMeshProUGUI mainhandSpeed;
    public TMPro.TextMeshProUGUI mainhandDPS;

    public TMPro.TextMeshProUGUI offhandDamage;
    public TMPro.TextMeshProUGUI offhandSpeed;
    public TMPro.TextMeshProUGUI offhandDPS;

    [SerializeField]
    private Character character;

    void Awake()
    {
        if (character != null)
        {
            character.onStatsChangedCallback += OnCharacterStatusChanged;
        }
    }

    public void SetCharacter(Character character)
    {
        if (this.character == character)
            return;

        this.character.onStatsChangedCallback -= OnCharacterStatusChanged;
        this.character = character;
        character.onStatsChangedCallback += OnCharacterStatusChanged;

        OnCharacterStatusChanged();
    }

    private void OnCharacterStatusChanged()
    {
        maxHealth.text = character.GetMaxHealth().ToString();
        healthRegen.text = character.GetHealthRegen().ToString();

        maxMana.text = character.GetMaxMana().ToString();
        manaRegen.text = character.GetManaRegen().ToString();
        armor.text = character.GetArmor().ToString();

        stamina.text = character.GetStamina().ToString();
        strength.text = character.GetStrength().ToString();
        intellect.text = character.GetIntellect().ToString();
        agility.text = character.GetAgility().ToString();
        spirit.text = character.GetSpirit().ToString();

        mainhandDamage.text = character.GetDamageMainhand().ToString();
        mainhandSpeed.text = character.GetAttackSpeedMainHand().ToString("0.00");
        mainhandDPS.text = (character.GetDamageMainhand().Average() * (1 / character.GetAttackSpeedMainHand())).ToString("0.0");

        offhandDamage.text = character.GetDamageOffhand().ToString();
        offhandSpeed.text = character.GetAttackSpeedOffHand().ToString("0.00");
        offhandDPS.text = (character.GetDamageOffhand().Average() * (1 / character.GetAttackSpeedOffHand())).ToString("0.0");
    }
}