﻿using UnityEngine;
using UnityEngine.EventSystems;

public abstract class DragAndDropContainer : MonoBehaviour
{
    public DragAndDropPanel panel;

    protected int containerIndex;

    void Start()
    {
        // Subscribe to the DragAndDropManager
        DragAndDropManager.instance.Subscribe(this);

        // Hide the panel in case it's vissible
        if (panel.gameObject.activeSelf)
            panel.gameObject.SetActive(false);
    }

    /// <summary>
    /// Set the container index of all panels
    /// </summary>
    /// <param name="containerIndex">The containers index</param>
    public void SetContainerIndex(int containerIndex)
    {
        this.containerIndex = containerIndex;
        Initialize();
    }

    /// <summary>
    /// Use this for initialization
    /// </summary>
    protected virtual void Initialize() { }

    /// <summary>
    /// Return the object in the given indices
    /// </summary>
    /// <param name="indices">The indeces in which the object is stored</param>
    /// <returns>The object in the given indices</returns>
    public abstract object GetObject(int[] indices, bool isFromContainer);

    /// <summary>
    /// Remove the object in the given indices
    /// </summary>
    /// <param name="indices">The indeces in which the object is stored</param>
    public abstract void RemoveObject(int[] indices);

    /// <summary>
    /// Try to recieve an object form another container
    /// </summary>
    /// <param name="o">The object to recieve</param>
    /// <param name="indices">The indeces in which the object is stored</param>
    /// <returns>If the container sucsessfully recieved the object</returns>
    public abstract bool RecieveObject(object o, int[] indices);

    /// <summary>
    /// Switch two objects in the same container
    /// </summary>
    /// <param name="indicesA">The indeces in which the first object is stored</param>
    /// <param name="IndicesB">The indeces in which the second object is stored</param>
    public virtual void InternalSwitch(int[] indicesA, int[] IndicesB)
    {
        // Get the object from the reciever
        object objectA = GetObject(indicesA, true);
        object objectB = GetObject(IndicesB, true);

        // If theres objects in both slots
        if (objectB != null && objectA != null)
        {
            // Remove the objects
            RemoveObject(IndicesB);
            RemoveObject(indicesA);

            // Try to switch the objects
            if (!RecieveObject(objectA, IndicesB) || !RecieveObject(objectB, indicesA))
            {
                // If any of them won't recieve the other object

                // Remove any objects from both
                RemoveObject(IndicesB);
                RemoveObject(indicesA);

                // Give them their own object back
                RecieveObject(objectB, IndicesB);
                RecieveObject(objectA, indicesA);
            }
        }
        else if (objectB != null)
        {
            // If the reciever sucsessfully recieved the object
            if (RecieveObject(objectB, indicesA))
                RemoveObject(IndicesB);
        }
        else if (objectA != null)
        {
            // If the reciever sucsessfully recieved the object
            if (RecieveObject(objectA, IndicesB))
                RemoveObject(indicesA);
        }
    }

    /// <summary>
    /// When an object is clicked
    /// </summary>
    /// <param name="button">The mouse button that was pressed</param>
    /// <param name="indices">The indeces in which the object is stored</param>
    public abstract void OnObjectMouseDown(PointerEventData.InputButton button, int[] indices);
}