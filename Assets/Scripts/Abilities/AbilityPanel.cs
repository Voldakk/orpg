﻿using UnityEngine.UI;

public class AbilityPanel : DragAndDropPanel
{
    public Image icon;
    public Image cooldown;
    public TMPro.TextMeshProUGUI abilityName;

    public Ability ability;

    public override void SetObject(object o)
    {
        if (o == null || !(o is Ability))
        {
            ability = null;
            icon.enabled = false;
            cooldown.enabled = false;
            if (abilityName != null)
                abilityName.text = "";
        }
        else
        {
            ability = o as Ability;

            icon.enabled = true;
            icon.sprite = ability.icon;

            if (abilityName != null)
                abilityName.text = ability.name;
        }
    }

    public Ability GetAbility()
    {
        return ability;
    }

    private void Awake()
    {
        cooldown.enabled = false;
        SetObject(ability);
    }
}