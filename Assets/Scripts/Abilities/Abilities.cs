﻿using UnityEngine;
using System.Collections.Generic;

[RequireComponent(typeof(Character))]
public class Abilities : MonoBehaviour
{
    public List<Ability> abilities = new List<Ability>();

    public Ability currentAbility;

    private Character character;

	// Use this for initialization
	void Start ()
    {
        character = GetComponent<Character>();
        character.onInterruptCallback += CharacterOnInterrupt;
    }

    // Update is called once per frame
    void Update()
    {
        for (int i = abilities.Count - 1; i >= 0; i--)
        {
            if (abilities[i] != null)
            {
                abilities[i].Update();

                if (abilities[i].state == AbilityState.InActive)
                {
                    if (abilities[i] == currentAbility)
                        currentAbility = null;

                    abilities.RemoveAt(i);
                }
            }
        }
    }

    /// <summary>
    /// Activate an ability
    /// </summary>
    /// <param name="ability">The ability</param>
    /// <returns>If the ability was activated</returns>
    public bool ActivateAbility(Ability ability)
    {
        if(!abilities.Contains(ability) && character.target is Character)
        {
            currentAbility = Instantiate(ability);

            currentAbility.Setup(character);
            currentAbility.Activate(character.target as Character);

            abilities.Add(currentAbility);

            return true;
        }
        else
        {
            return false;
        }
    }

    /// <summary>
    /// Called when the characters interrupts other actions
    /// </summary>
    private void CharacterOnInterrupt()
    {
        foreach (Ability ability in abilities)
        {
            ability.OnInterrupt();
        }
    }
}