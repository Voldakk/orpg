﻿using UnityEngine;
using UnityEngine.EventSystems;

public class AbilityBookUI : DragAndDropContainer
{
    private AbilityPanel[] abilityPanels;

    [SerializeField] private Abilities abilities;

    [SerializeField] private Transform grid;
    [SerializeField] private GameObject panelPrefab;

    protected override void Initialize()
    {
        base.Initialize();

        // Clear the list
        for (int i = 0; i < grid.childCount; i++)
        {
            Destroy(grid.GetChild(i).gameObject);
        }

        // Load all abilities from file
        Ability[] abilityList = Resources.LoadAll<Ability>("Abilities");
        abilityPanels = new AbilityPanel[abilityList.Length];

        for (int i = 0; i < abilityList.Length; i++)
        {
            abilityPanels[i] = Instantiate(panelPrefab, grid).GetComponent<AbilityPanel>();
            abilityPanels[i].SetIndeces(containerIndex, new int[] { i });
            abilityPanels[i].SetObject(abilityList[i]);
        }
    }

    public override object GetObject(int[] indices, bool isFromContainer)
    {
        if (indices.Length != 1)
            return null;

        if (isFromContainer)
            return abilityPanels[indices[0]].GetAbility();
        else
            return null; // Don't attempt to switch when draging an ability into the book
    }

    public override void OnObjectMouseDown(PointerEventData.InputButton button, int[] indices)
    {
        if (indices.Length != 1)
            return;

        if (abilityPanels[indices[0]].GetAbility() != null)
            abilities.ActivateAbility(abilityPanels[indices[0]].GetAbility());
    }

    public override bool RecieveObject(object o, int[] indices)
    {
        if (indices.Length != 1)
            return false;

        if (!(o is Ability))
        {
            Debug.Log("Invalid item");
            return false;
        }

        return true;
    }

    public override void RemoveObject(int[] indices) { } // Nope

    private void Update()
    {
        // Ability panels
        foreach (AbilityPanel panel in abilityPanels)
        {
            if (panel.GetAbility() == null)
                continue;

            foreach (Ability ability in abilities.abilities)
            {
                if (ability == null)
                    continue;

                if (ability.Equals(panel.GetAbility()))
                {
                    // Cooldown
                    if (ability.state == AbilityState.Cooldown)
                    {
                        panel.cooldown.enabled = true;
                        panel.cooldown.fillAmount = ability.cooldownTimeRemaining / ability.cooldownTime;
                    }

                    // Other things

                    //Break
                    break;
                }

                // Defaults
                panel.cooldown.enabled = false;
            }
        }
    }
}