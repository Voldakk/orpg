﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class AbilitiesUI : DragAndDropContainer
{
    private AbilityPanel[] abilityPanels;

    [SerializeField] private Abilities abilities;

    [SerializeField] private Slider activateBar;
    [SerializeField] private TMPro.TextMeshProUGUI activateText;

    /// <summary>
    /// Use this for initialization
    /// </summary>
    protected override void Initialize()
    {
        base.Initialize();

        abilityPanels = new AbilityPanel[transform.childCount];

        for (int i = 0; i < transform.childCount; i++)
        {
            abilityPanels[i] = transform.GetChild(i).GetComponent<AbilityPanel>();
            abilityPanels[i].SetIndeces(containerIndex, new int[] { i });
        }

        activateBar.gameObject.SetActive(false);
    }

    public override object GetObject(int[] indices, bool isFromContainer)
    {
        if (indices.Length != 1)
            return null;

        return abilityPanels[indices[0]].GetAbility();
    }

    public override void OnObjectMouseDown(PointerEventData.InputButton button, int[] indices)
    {
        if (indices.Length != 1)
            return;

        if (abilityPanels[indices[0]].GetAbility() != null)
            abilities.ActivateAbility(abilityPanels[indices[0]].GetAbility());
    }

    public override bool RecieveObject(object o, int[] indices)
    {
        if (indices.Length != 1)
            return false;

        if (!(o is Ability))
        {
            Debug.Log("Invalid item");
            return false;
        }

        if (abilityPanels[indices[0]].GetAbility() != null)
        {
            Debug.Log("Already an item in the slot");
            return false;
        }

        abilityPanels[indices[0]].SetObject(o as Ability);
        return true;
    }

    public override void RemoveObject(int[] indices)
    {
        if (indices.Length != 1)
            return;

        abilityPanels[indices[0]].SetObject(null);
    }

    /// <summary>
    /// Called every frame
    /// </summary>
    private void Update()
    {
        // Active bar
        if (abilities.currentAbility != null && abilities.currentAbility.state == AbilityState.Activating)
        {
            if (!activateBar.gameObject.activeSelf)
                activateBar.gameObject.SetActive(true);

            activateBar.maxValue = abilities.currentAbility.activationTime;
            activateBar.value = abilities.currentAbility.activationTime - abilities.currentAbility.activationTimeRemaining;
        }
        else
        {
            if (activateBar.gameObject.activeSelf)
                activateBar.gameObject.SetActive(false);
        }

        // Ability panels
        foreach (AbilityPanel panel in abilityPanels)
        {
            if (panel.GetAbility() == null)
                continue;

            foreach (Ability ability in abilities.abilities)
            {
                if (ability == null)
                    continue;

                if (ability.Equals(panel.GetAbility()))
                {
                    // Cooldown
                    if (ability.state == AbilityState.Cooldown)
                    {
                        panel.cooldown.enabled = true;
                        panel.cooldown.fillAmount = ability.cooldownTimeRemaining / ability.cooldownTime;
                    }

                    // Other things

                    //Break
                    break;
                }

                // Defaults
                panel.cooldown.enabled = false;
            }
        }
    }
}