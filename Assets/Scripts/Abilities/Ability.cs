﻿using System.Collections.Generic;
using UnityEngine;

public enum AbilityState { InActive, Activating, Active, Cooldown }

[CreateAssetMenu(fileName = "New Ability", menuName = "Abilities/Ability")]
public abstract class Ability : ScriptableObject
{
    new public string name;
    public Sprite icon;

    public float activationTime;
    public float activationTimeRemaining = 0;
    public float lastActivatedTime;

    public float cooldownTime;
    public float cooldownTimeRemaining = 0;
    public float lastUseTime;

    public AbilityState state = AbilityState.InActive;

    protected Character character;
    protected Character target;

    /// <summary>
    /// Use this for initialization
    /// </summary>
    public void Setup(Character self)
    {
        this.character = self;
    }

    /// <summary>
    /// Activate the ability
    /// </summary>
    /// <param name="target"></param>
    public void Activate(Character target)
    {
        if (state != AbilityState.InActive)
            return;

        Debug.Log("Activated ability: " + name, character);

        if (ValidTarget(target))
        {
            this.target = target;
            lastActivatedTime = Time.time;
            state = AbilityState.Activating;
        }
        else
        {
            Debug.Log("Invalid target for ability: " + name, target );
        }
    }

    /// <summary>
    /// Triggers every frame
    /// </summary>
    public void Update()
    {
        if(state == AbilityState.Activating)
        {
            activationTimeRemaining = lastActivatedTime + activationTime - Time.time;
            if (activationTimeRemaining <= 0)
            {
                lastUseTime = Time.time;
                state = AbilityState.Active;

                OnActivated();
            }
        }
        else if (state == AbilityState.Active)
        {
            WhileActive();
        }
        else if (state == AbilityState.Cooldown)
        {
            cooldownTimeRemaining = lastUseTime + cooldownTime - Time.time;
            if (cooldownTimeRemaining <= 0)
            {
                state = AbilityState.InActive;
            }
        }
    }

    /// <summary>
    /// Called when the ability is first fired
    /// </summary>
    protected abstract void OnActivated();

    /// <summary>
    /// Called every frame the ability is active
    /// </summary>
    protected abstract void WhileActive();

    /// <summary>
    /// Triggers when interrupted by another ability or the ability is otherwise canceled
    /// </summary>
    public virtual void OnInterrupt()
    {
        if(state == AbilityState.Activating)
        {
            state = AbilityState.InActive;
        }
    }

    /// <summary>
    /// If the target is a valid target for this ability
    /// </summary>
    /// <param name="target">The target</param>
    /// <returns>Whether the target is valid</returns>
    public abstract bool ValidTarget(Character target);

    protected void Done()
    {
        state = AbilityState.Cooldown;
    }

    public override bool Equals(object other)
    {
        if (other is Ability)
            return name == (other as Ability).name;
        else
            return false;
    }

    /// <summary>
    /// Auto generated GetHashCode
    /// </summary>
    /// <returns>Int hashCode</returns>
    public override int GetHashCode()
    {
        var hashCode = -1925595674;
        hashCode = hashCode * -1521134295 + base.GetHashCode();
        hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(name);
        return hashCode;
    }
}