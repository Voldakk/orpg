﻿using UnityEngine;

[CreateAssetMenu(fileName = "New AbilityInstantDamage", menuName = "Abilities/AbilityInstantDamage")]
public class AbilityInstantDamage : Ability
{
    public Range baseDamage;

    public GameObject effectPrefab;

    public override void OnInterrupt()
    {
        
    }

    public override bool ValidTarget(Character target)
    {
        return true;
    }

    protected override void WhileActive()
    {
        
    }

    protected override void OnActivated()
    {
        Range damage = baseDamage + character.GetSpellPower();

        target.TakeDamage(damage.Random());

        Instantiate(effectPrefab, target.transform.position, Quaternion.identity);

        Done();
    }
}
