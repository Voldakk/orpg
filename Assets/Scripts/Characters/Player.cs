﻿using UnityEngine;

[RequireComponent(typeof(PlayerEquipment))]
public class Player : Character
{
    public LevelXP levelXP;
    int xp = 0;

    protected override void Start()
    {
        base.Start();

        (equipment as PlayerEquipment).playerCharacter = this;
    }

    /// <summary>
    /// Gets the player current xp
    /// </summary>
    /// <returns>The amount of xp</returns>
    public int GetCurrentXP()
    {
        return xp;
    }

    /// <summary>
    /// Gets the required xp to level up
    /// </summary>
    /// <returns>The amount of xp</returns>
    public int GetNextLevelXP()
    {
        if (level >= levelXP.MaxLevel())
            return levelXP.Get(level + 1);
        
        return -1;
    }

    /// <summary>
    /// Gets the max level
    /// </summary>
    /// <returns>The level</returns>
    public int GetMaxLevel()
    {
        return levelXP.MaxLevel();
    }

    /// <summary>
    /// Give the player an amount of xp
    /// </summary>
    /// <param name="amount">The amount</param>
    public void GiveXP(int amount)
    {
        // If the amount is negative
        if(amount < 0)
        {
            Debug.LogWarningFormat("Player.GiveXP: Trying to give the player a negative amount of xp ({0})", amount);
            return;
        }

        // If the player is already at the max level
        if (level >= levelXP.MaxLevel())
            return;

        // Apply the amount
        xp += amount;

        // Get the required xp for the next level
        int nextLevelXP = levelXP.Get(level + 1);

        // If the player has enough xp
        if(xp > nextLevelXP)
        {
            int remainingXP = xp - nextLevelXP;

            xp = 0;
            level++;
            LevelUp();

            // The the plater isn't at the max level
            if (level < levelXP.MaxLevel())
            {
                GiveXP(remainingXP);
            }
        }
        
    }

    /// <summary>
    /// Called when the player level up
    /// </summary>
    private void LevelUp()
    {
        Debug.Log("Yay!");
    }
}
