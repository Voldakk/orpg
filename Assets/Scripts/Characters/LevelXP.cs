﻿using UnityEngine;

[CreateAssetMenu(fileName = "LevelXP", menuName = "Data/LevelXP")]
public class LevelXP : ScriptableObject
{
    [SerializeField]
    private AnimationCurve curve;

    public int Get(int level)
    {
        return Mathf.RoundToInt(curve.Evaluate(level));
    }

    public int MaxLevel()
    {
        return Mathf.RoundToInt(curve.keys[curve.length - 1].time);
    }
}
