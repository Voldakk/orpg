﻿using System;
using UnityEngine;

public class Equipment : MonoBehaviour
{ 
    // Equipment array
    protected ItemEquipment[] equipment = new ItemEquipment[Enum.GetNames(typeof(EquipmentSlot)).Length];

    // The number of equipment slots
    public int numberOfSlots { get { return equipment == null ? 0 : equipment.Length; } }

    // Callback for when the equipment has changed
    public delegate void OnEquipmentChanged(ItemEquipment from, ItemEquipment to);
    public OnEquipmentChanged onEquipmentChangedCallback;

    protected void OnEquipmentChangedCallback(ItemEquipment from, ItemEquipment to)
    {
        if (onEquipmentChangedCallback != null)
            onEquipmentChangedCallback.Invoke(from, to);
        else
            Debug.LogWarning("No subsicibers for onItemChangedCallback");
    }

    /// <summary>
    /// Get the item in a slot
    /// </summary>
    /// <param name="slot">The slot</param>
    /// <returns>The item. Returns null if there's no item in the slot</returns>
    public ItemEquipment GetEquipment(EquipmentSlot slot)
    {
        return GetEquipment((int)slot);
    }

    /// <summary>
    /// Get the item in a slot
    /// </summary>
    /// <param name="slot">The slot</param>
    /// <returns>The item. Returns null if there's no item in the slot</returns>
    public ItemEquipment GetEquipment(int slot)
    {
        return equipment[slot];
    }
}
