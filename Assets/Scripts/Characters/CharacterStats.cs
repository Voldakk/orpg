﻿using UnityEngine;



[CreateAssetMenu(fileName = "New CharacterStats", menuName = "Characters/Stats")]
public class CharacterStats : ScriptableObject
{
    [SerializeField] private LevelRange baseDamage;
    [SerializeField] private AnimationCurve attackSpeed;

    [SerializeField] private AnimationCurve baseStamina;
    [SerializeField] private AnimationCurve baseStrength;
    [SerializeField] private AnimationCurve baseIntellect;
    [SerializeField] private AnimationCurve baseAgility;
    [SerializeField] private AnimationCurve baseSpirit;

    /// <summary>
    /// Returns the characters base damage for the given level
    /// </summary>
    /// <param name="level">The characters level</param>
    /// <returns>The base damage for the given level</returns>
    public Range GetBaseDamage(int level)
    {
        return baseDamage.GetRange(level);
    }

    /// <summary>
    /// Returns the characters base attack speed for the given level
    /// </summary>
    /// <param name="level">The characters level</param>
    /// <returns>The base attack speed for the given level</returns>
    public float GetAttackSpeed(int level)
    {
        return attackSpeed.Evaluate(level);
    }

    /// <summary>
    /// Returns the characters base stamina for the given level
    /// </summary>
    /// <param name="level">The characters level</param>
    /// <returns>The base stamina for the given level</returns>
    public int GetBaseStamina(int level)
    {
        return Mathf.RoundToInt(baseStamina.Evaluate(level));
    }

    /// <summary>
    /// Returns the characters base strength for the given level
    /// </summary>
    /// <param name="level">The characters level</param>
    /// <returns>The base strength for the given level</returns>
    public int GetBaseStrength(int level)
    {
        return Mathf.RoundToInt(baseStrength.Evaluate(level));
    }

    /// <summary>
    /// Returns the characters base intellect for the given level
    /// </summary>
    /// <param name="level">The characters level</param>
    /// <returns>The base intellect for the given level</returns>
    public int GetBaseIntellect(int level)
    {
        return Mathf.RoundToInt(baseIntellect.Evaluate(level));
    }

    /// <summary>
    /// Returns the characters base agility the given level
    /// </summary>
    /// <param name="level">The characters level</param>
    /// <returns>The base agility for the given level</returns>
    public int GetBaseAgility(int level)
    {
        return Mathf.RoundToInt(baseAgility.Evaluate(level));
    }

    /// <summary>
    /// Returns the characters base spirit the given level
    /// </summary>
    /// <param name="level">The characters level</param>
    /// <returns>The base spirit for the given level</returns>
    public int GetBaseSpirit(int level)
    {
        return Mathf.RoundToInt(baseSpirit.Evaluate(level));
    }
}