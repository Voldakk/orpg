﻿using UnityEngine;

public enum Stance { Friendly, Neutral, Hostile }

[RequireComponent(typeof(Equipment))]
public class Character : Interactable
{
    public Stance stance;
    public CharacterStats baseStats;
    public Equipment equipment;

    private Interactable _target;
    public Interactable target { get { return _target; } set { _target = value; OnTargetChangedCallback(); } }

    [Range(1, 100)]
    public int level = 1;

    //The current stats
    protected int health;
    protected int mana;
    protected int armor;
    protected int stamina;
    protected int strength;
    protected int intellect;
    protected int agility;
    protected int spirit;

    // Derived stats
    protected int maxHealth;
    protected int maxMana;
    protected int healthRegen;  // health per second
    protected int manaRegen;    // mana per second
    protected float attackSpeed;
    protected int spellPower;

    // Callback for when the characters stats are changed
    public delegate void OnStatsChanged();
    public OnStatsChanged onStatsChangedCallback;

    /// <summary>
    /// Called when the characters stats are changed
    /// </summary>
    public void OnStatsChangedCallback()
    {
        if (onStatsChangedCallback != null)
            onStatsChangedCallback.Invoke();
    }

    // Callback for when the character interrups other actions
    public delegate void OnInterrupt();
    public OnStatsChanged onInterruptCallback;

    /// <summary>
    /// Called when the characters should interrupt other actions
    /// </summary>
    public void OnInterruptCallback()
    {
        if (onInterruptCallback != null)
            onInterruptCallback.Invoke();
    }

    // Callback for when the characters stats are changed
    public delegate void OnTargetChanged();
    public OnTargetChanged onTargetChangedCallback;

    /// <summary>
    /// Called when the characters stats are changed
    /// </summary>
    public void OnTargetChangedCallback()
    {
        if (onTargetChangedCallback != null)
            onTargetChangedCallback.Invoke();
    }

    protected virtual void Awake()
    {
        equipment = GetComponent<Equipment>();
        equipment.onEquipmentChangedCallback += OnEquipmentChanged;
    }

    protected virtual void Start()
    {
        GetAllEquipmentStats();
        health = GetMaxHealth();
        mana = GetMaxMana();
        OnStatsChangedCallback();
    }

    /// <summary>
    /// Returns the characters current health
    /// </summary>
    /// <returns>The characters current health</returns>
    public int GetCurrentHealth()
    {
        return health;
    }

    /// <summary>
    /// Returns the characters max health at it's current level
    /// </summary>
    /// <returns>The characters max health</returns>
    public int GetMaxHealth()
    {
        return maxHealth;
    }

    /// <summary>
    /// Returns the characters health regen at it's current level
    /// </summary>
    /// <returns>The characters health regen</returns>
    public int GetHealthRegen()
    {
        return healthRegen;
    }

    /// <summary>
    /// Returns the characters current mana
    /// </summary>
    /// <returns>The characters current mana</returns>
    public int GetCurrentMana()
    {
        return mana;
    }

    /// <summary>
    /// Returns the characters max mana at it's current level
    /// </summary>
    /// <returns>The characters max mana</returns>
    public int GetMaxMana()
    {
        return maxMana;
    }

    /// <summary>
    /// Returns the characters mana regen at it's current level
    /// </summary>
    /// <returns>The characters mana regen</returns>
    public int GetManaRegen()
    {
        return manaRegen;
    }

    /// <summary>
    /// Returns the characters armor
    /// </summary>
    /// <returns>The characters armor</returns>
    public int GetArmor()
    {
        return armor;
    }

    /// <summary>
    /// Returns the characters stamina
    /// </summary>
    /// <returns>The characters stamina</returns>
    public int GetStamina()
    {
        return baseStats.GetBaseStamina(level) + stamina;
    }

    /// <summary>
    /// Returns the characters strength
    /// </summary>
    /// <returns>The characters strength</returns>
    public int GetStrength()
    {
        return baseStats.GetBaseStrength(level) + strength;
    }

    /// <summary>
    /// Returns the characters intellect
    /// </summary>
    /// <returns>The characters intellect</returns>
    public int GetIntellect()
    {
        return baseStats.GetBaseIntellect(level) + intellect;
    }

    /// <summary>
    /// Returns the characters agility
    /// </summary>
    /// <returns>The characters agility</returns>
    public int GetAgility()
    {
        return baseStats.GetBaseAgility(level) + agility;
    }

    /// <summary>
    /// Returns the characters spirit
    /// </summary>
    /// <returns>The characters spirit</returns>
    public int GetSpirit()
    {
        return baseStats.GetBaseSpirit(level) + spirit;
    }

    /// <summary>
    /// Return the characters damage range with the main hand
    /// </summary>
    /// <returns>The damage range</returns>
    public Range GetDamageMainhand()
    {
        Range damage = new Range(strength);

        if (equipment.GetEquipment(EquipmentSlot.MainHand) == null)
            damage += baseStats.GetBaseDamage(level);
        else
            damage += (equipment.GetEquipment(EquipmentSlot.MainHand) as EquipmentWeapon).damage;

        return damage;
    }

    /// <summary>
    /// Returns the characters attack speed with the main hand
    /// </summary>
    /// <returns>The characters attack speed</returns>
    public float GetAttackSpeedMainHand()
    {
        float speed;

        if (equipment.GetEquipment(EquipmentSlot.MainHand) == null)
            speed = baseStats.GetAttackSpeed(level) * attackSpeed;
        else
            speed = (equipment.GetEquipment(EquipmentSlot.MainHand) as EquipmentWeapon).attackSpeed * attackSpeed;

        return speed;
    }

    /// <summary>
    /// Return the characters damage range with the main hand
    /// </summary>
    /// <returns>The damage range</returns>
    public Range GetDamageOffhand()
    {
        Range damage = new Range(strength);

        if (equipment.GetEquipment(EquipmentSlot.OffHand) == null)
            damage += baseStats.GetBaseDamage(level);
        else
            damage += (equipment.GetEquipment(EquipmentSlot.OffHand) as EquipmentWeapon).damage;

        return damage;
    }

    /// <summary>
    /// Returns the characters attack speed with the main hand
    /// </summary>
    /// <returns>The characters attack speed</returns>
    public float GetAttackSpeedOffHand()
    {
        float speed;

        if (equipment.GetEquipment(EquipmentSlot.OffHand) == null)
            speed = baseStats.GetAttackSpeed(level) * attackSpeed;
        else
            speed = (equipment.GetEquipment(EquipmentSlot.OffHand) as EquipmentWeapon).attackSpeed * attackSpeed;

        return speed;
    }

    /// <summary>
    /// Returns the charaters spell power
    /// </summary>
    /// <returns>The characters spell power</returns>
    public int GetSpellPower()
    {
        return spellPower;
    }

    /// <summary>
    /// Damage the character
    /// </summary>
    /// <param name="value">The amount of damage</param>
    public void TakeDamage(int value)
    {
        if(value <= 0 )
        {
            Debug.LogErrorFormat("Trying to deal {0} damage to character {1}", value, gameObject.name);
            return;
        }
        
        int damage = value - GetArmor();
        if(damage > 0)
        {
            health -= damage;
            if(health <= 0)
            {
                Die();
            }
        }

        OnStatsChangedCallback();
    }

    /// <summary>
    /// Kills the character
    /// </summary>
    public virtual void Die()
    {
        health = 0;

        Destroy(gameObject);
    }

    /// <summary>
    /// Updates the character stats based on the changed equipment
    /// </summary>
    /// <param name="from">The old equipmnent</param>
    /// <param name="to">The new equipment</param>
    private void OnEquipmentChanged(ItemEquipment from, ItemEquipment to)
    {
        OnEquipmentChanged(from, to, true);
    }
    private void OnEquipmentChanged(ItemEquipment from, ItemEquipment to, bool callback)
    {
        if (from != null)
        {
            stamina -= from.stamina;
            strength -= from.strength;
            intellect -= from.intellect;
            agility -= from.agility;
            spirit -= from.spirit;

            if(from is EquipmentArmor)
            {
                armor -= (from as EquipmentArmor).armor;
            }
        }
        if (to != null)
        {
            stamina += to.stamina;
            strength += to.strength;
            intellect += to.intellect;
            agility += to.agility;
            spirit += spirit;

            if (to is EquipmentArmor)
            {
                armor += (to as EquipmentArmor).armor;
            }
        }

        // Derrived stats
        maxHealth = GetStamina() * 10;
        healthRegen = GetSpirit() + GetStamina() / 5;

        maxMana = GetIntellect() * 10;
        manaRegen = GetSpirit() + GetIntellect() / 5;

        attackSpeed =  baseStats.GetBaseAgility(level) / (float)GetAgility();
        spellPower = GetIntellect() + GetSpirit() / 5;

        // Clamp health
        if (health > GetMaxHealth())
            health = GetMaxHealth();

        // Callback
        if (callback)
            OnStatsChangedCallback();
    }

    /// <summary>
    /// Resets the stats and reads the stats from all current equipment
    /// </summary>
    private void GetAllEquipmentStats()
    {
        armor = 0;
        stamina = 0;
        strength = 0;
        intellect = 0;
        agility = 0;
        spirit = 0;

        for (int i = 0; i < equipment.numberOfSlots; i++)
        {
            OnEquipmentChanged(null, equipment.GetEquipment(i), false);
        }

        OnStatsChangedCallback();
    }
}