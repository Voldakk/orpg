﻿using UnityEngine;
using Cinemachine;
using UnityStandardAssets.Characters.ThirdPerson;
using UnityStandardAssets.CrossPlatformInput;
using UnityEngine.EventSystems;

[RequireComponent(typeof(ThirdPersonCharacter))]
[RequireComponent(typeof(Player))]
public class PlayerController : MonoBehaviour
{
    [SerializeField] CinemachineFreeLook cfl;

    [SerializeField] float runSpeedMultiplier = 2;    // Run speed multiplier
    [SerializeField] float turnSpeed = 90;            // Character turn speed  ( deg/sec )
    [SerializeField] float recenterSpeed = 360;       // Camera recenter speed ( deg/sec )

    ThirdPersonCharacter character; // A reference to the ThirdPersonCharacter on the object
    Transform cam;                  // A reference to the main camera in the scenes transform
    Vector3 camForward;             // The current forward direction of the camera

    Vector3 movement;               // The world-relative desired move direction
    float turnAmount;               // Player rotation

    bool jump, run, crouch;         // If we're currently jumping, running or crouching

    Quaternion direction;           // Current movement direction in deg

    bool mouse0, mouse1;            // If the mouse buttons are currently held down
    Vector3 onPressMousePosition0, onPressMousePosition1;

    public Interactable target;
    public Interactable hover;
    public Interactable lastHover;

    private Player player;
    private Rigidbody rb;

    // Callback for when the hover is changed
    public delegate void OnHoverChanged();
    public OnHoverChanged onHoverChangedCallback;

    /// <summary>
    /// Called when the hover is changed
    /// </summary>
    private void OnHoverChangedCallback()
    {
        if (onHoverChangedCallback != null)
            onHoverChangedCallback.Invoke();
    }

    private void Start()
    {
        Cursor.lockState = CursorLockMode.Confined;

        // Get the transform of the main camera
        if (Camera.main != null)
            cam = Camera.main.transform;
        else
            Debug.LogWarning("Warning: no main camera found. The PlayerController script needs a Camera tagged \"MainCamera\"", gameObject);

        // Get the third person character ( this should never be null due to require component )
        character = GetComponent<ThirdPersonCharacter>();

        // Recenter camera
        direction = Quaternion.AngleAxis(transform.rotation.eulerAngles.y, Vector3.up);
        cfl.m_XAxis.Value = Mathf.Repeat(direction.eulerAngles.y, 360);

        // Get the player
        player = GetComponent<Player>();
        rb = GetComponent<Rigidbody>();
    }

    void Update ()
    {
        // Read inputs
        if (!EventSystem.current.IsPointerOverGameObject())
        {
            if (Input.GetMouseButtonDown(0))
            {
                mouse0 = true;
                onPressMousePosition0 = Input.mousePosition;
            }
            if (Input.GetMouseButtonDown(1))
            {
                mouse1 = true;
                onPressMousePosition1 = Input.mousePosition;
            }
        }

        if (!Input.GetMouseButton(0) && mouse0)
        {
            mouse0 = false;

            if(Vector3.Distance(Input.mousePosition, onPressMousePosition0) < 5)
                Click0();
        }
        if (!Input.GetMouseButton(1) && mouse1)
        {
            mouse1 = false;

            if (Vector3.Distance(Input.mousePosition, onPressMousePosition0) < 5)
                Click1();
        }

        crouch = false; //Input.GetKey(KeyCode.C);
        run = Input.GetKey(KeyCode.LeftShift);

        if (!jump)
            jump = CrossPlatformInputManager.GetButtonDown("Jump");

        // Allow camera rotation if one of the mouse buttons are pressed
        if(mouse0 || mouse1)
        {
            Cursor.visible = false;
            cfl.m_XAxis.m_InputAxisValue = - Input.GetAxis("Mouse X");
            cfl.m_YAxis.m_InputAxisValue = Input.GetAxis("Mouse Y");
        }
        else // Recenter the camera
        {
            Cursor.visible = true;
            cfl.m_XAxis.m_InputAxisValue = 0;
            cfl.m_YAxis.m_InputAxisValue = 0;

            cfl.m_XAxis.Value = Mathf.MoveTowardsAngle(cfl.m_XAxis.Value, Mathf.Repeat(direction.eulerAngles.y, 360), Time.deltaTime * recenterSpeed);
        }

        // Toggle cursor lock state
        if (Input.GetKeyDown(KeyCode.Escape))
                Cursor.lockState = Cursor.lockState == CursorLockMode.Confined ? CursorLockMode.None : CursorLockMode.Confined;

        // Interaction
        lastHover = hover;
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;
        if (!EventSystem.current.IsPointerOverGameObject() && Physics.Raycast(ray, out hit, 1000))
        {
            hover = hit.transform.GetComponent<Interactable>();
        }

        if (hover != lastHover)
            OnHoverChangedCallback();

        // Check movement
        if (rb.velocity.magnitude > 1)
            player.OnInterruptCallback();
    }

    private void Click0()
    {
        if (hover != null)
        {
            if (target != null)
                target.OnDefocused();

            target = hover;
            target.OnFocused(this);
        }
        else
        {
            if (target != null)
                target.OnDefocused();

            target = null;
        }

        if (player.target != target)
            player.target = target;
    }

    private void Click1()
    {
        if (hover != null)
        {
            if (target != null)
                target.OnDefocused();

            target = hover;
            target.OnFocused(this);
            target.OnActivate();
        }
        else
        {
            if (target != null)
                target.OnDefocused();

                target = null;
        }

        if (player.target != target)
            player.target = target;
    }

    // Fixed update is called in sync with physics
    private void FixedUpdate()
    {
        // Read inputs
        float h = CrossPlatformInputManager.GetAxis("Horizontal");
        float v = CrossPlatformInputManager.GetAxis("Vertical");

        // Always move forward if both mouse buttons are pressed
        if (mouse0 && mouse1)
            v = 1;

        // If the right mouse button is pressed the character should turn towards the camera's forward direction
        if (mouse1)
        {
            camForward = Vector3.Scale(cam.forward, new Vector3(1, 0, 1)).normalized;
            direction = Quaternion.LookRotation(camForward, Vector3.up);
        }
        else // Move in the characters forward direction
        {
            direction *= Quaternion.AngleAxis(h * turnSpeed * Time.fixedDeltaTime, Vector3.up);
        }

        movement = direction * Vector3.forward * v;

        // Run
        if (run) movement *= runSpeedMultiplier;

        // Find the differrence between the character's and our actual direction
        turnAmount = direction.eulerAngles.y - transform.rotation.eulerAngles.y;
        // Wrap rotation to -180..180
        if (turnAmount > 180)
            turnAmount -= 360;
        else if (turnAmount < -180)
            turnAmount += 360;
        

        // Pass all parameters to the character control script
        character.Move(movement, turnAmount * Mathf.Deg2Rad, crouch, jump);
        jump = false;
    }
}
