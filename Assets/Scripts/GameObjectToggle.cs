﻿using UnityEngine;

public class GameObjectToggle : MonoBehaviour
{
    public GameObject[] targets;
    public KeyCode key;
    public bool startActive;

	void Start ()
    {
        for (int i = 0; i < targets.Length; i++)
        {
            targets[i].SetActive(startActive);
        }
	}
	
	void Update ()
    {
        if (Input.GetKeyDown(key))
        {
            for (int i = 0; i < targets.Length; i++)
            {
                targets[i].SetActive(!targets[i].activeSelf);
            }
        }
    }
}