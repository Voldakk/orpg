﻿using UnityEngine;
using System;

public class PlayerEquipment : Equipment
{
    // Singleton
    public static PlayerEquipment instance;

    // Refference to the player inventory
    private PlayerInventory inventory;

    // Refference to the player mesh
    public SkinnedMeshRenderer targetMesh;

    public Player playerCharacter;

    /// <summary>
    /// Called before Start
    /// </summary>
    void Awake()
    {
        // Singleton
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Debug.LogError("More than one PlayerEquipment! " + gameObject.name);
            Destroy(this);
            return;
        }
    }

    /// <summary>
    /// Called before the first Update
    /// </summary>
    void Start()
    {
        inventory = PlayerInventory.instance;
    }

    /// <summary>
    /// Equip an item and remove it from the inventory
    /// </summary>
    /// <param name="item">The item to be equipped</param>
    /// <param name="slot">The slot</param>
    /// <returns>Wheter the item was succsessfully equipped</returns>
    public bool EquipItemFromInventory(Item item, EquipmentSlot slot)
    {
        return EquipItemFromInventory(item, (int)slot);
    }

    /// <summary>
    /// Equip an item and remove it from the inventory
    /// </summary>
    /// <param name="item">The item to be equipped</param>
    /// <param name="slot">The slot</param>
    /// <returns>Wheter the item was succsessfully equipped</returns>
    public bool EquipItemFromInventory(Item item, int slot)
    {
        // If the player can equip the item in the specified slot
        if (!CanEquip(item, slot))
            return false;


        ItemEquipment newEquipment = item as ItemEquipment;

        // Cache the items inventory position
        int inventoryBagIndex = newEquipment.inventoryBagIndex;
        int inventoryItemIndex = newEquipment.inventoryItemIndex;

        // Remove the item from the inventory
        if (!newEquipment.RemoveFromInventory())
            Debug.LogFormat("PlayerEquipment.Equip: Failed to remove item: {0}, Bag:{1}, Item:{2}, In:{3}", newEquipment.name, newEquipment.inventoryBagIndex, newEquipment.inventoryItemIndex, newEquipment.inInventory);

        // If there's currently an item in the slot
        ItemEquipment oldItem = null;
        if (equipment[slot] != null)
        {
            oldItem = equipment[slot];

            // Add it to the inventory at the new item's possition
            inventory.Add(equipment[slot], inventoryBagIndex, inventoryItemIndex);
            Unequip(slot);
        }

        // Equip the new item
        Equip(newEquipment, slot);

        // The equipment has changed
        OnEquipmentChangedCallback(oldItem, item as ItemEquipment);

        return true;
    }

    /// <summary>
    /// Equip an item
    /// </summary>
    /// <param name="item">The item to be equipped</param>
    /// <param name="slot">The slot</param>
    /// <returns>Wheter the item was succsessfully equipped</returns>
    public bool EquipItem(Item item, int slot)
    {
        // If the player can equip the item in the specified slot
        if (!CanEquip(item, slot))
        return false;


        ItemEquipment newEquipment = item as ItemEquipment;

        // If there's currently an item in the slot
        ItemEquipment oldItem = null;
        if (equipment[slot] != null)
        {

            oldItem = equipment[slot];
            // Add it to the inventory
            inventory.Add(equipment[slot]);
            Unequip(slot);
        }

        // Equip the new item
        Equip(newEquipment, slot);

        // The equipment has changed
        OnEquipmentChangedCallback(oldItem, item as ItemEquipment);
        return true;
    }

    /// <summary>
    /// Unequip the item in the slot
    /// </summary>
    /// <param name="slot">The slot</param>
    public void UnequipItem(int slot)
    {
        if (equipment[slot] == null)
            return;

        ItemEquipment oldItem = equipment[slot];
        Unequip(slot);

        // The equipment has changed
        OnEquipmentChangedCallback(oldItem, null);
    }

    /// <summary>
    /// Unequip the item in the slot and try adding it to the inventory
    /// </summary>
    /// <param name="slot">The slot</param>
    /// <returns>Wheter the item was sucsessfully unequiped</returns>
    public bool UnequipItemToInventory(int slot)
    {
        if (equipment[slot] == null)
            return true;


        if (inventory.Add(equipment[slot]))
        {
            ItemEquipment oldItem = equipment[slot];
            Unequip(slot);

            // The equipment has changed
            OnEquipmentChangedCallback(oldItem, null);

            return true;
        }

        return false;
    }

    private void Equip(ItemEquipment item, int slot)
    {
        // Add the item to the slot
        equipment[slot] = item;

        // Instantiate the mesh
        item.mesh = Instantiate<SkinnedMeshRenderer>(item.prefab, targetMesh.transform);

        // Set the bones
        item.mesh.rootBone = targetMesh.rootBone;
        item.mesh.bones = targetMesh.bones;

        // Set it to the correct layer
        item.mesh.gameObject.layer = gameObject.layer;

        // Set the blend shapes
        SetEquipmentBlendShapes(item, 100);
    }

    private void Unequip(int slot)
    {
        // If there's an item in the slot
        if(equipment[slot] != null)
        {
            // Destroy the mesh
            if (equipment[slot].mesh != null)
                Destroy(equipment[slot].mesh.gameObject);

            // Set the blend shapes
            SetEquipmentBlendShapes(equipment[slot], 0);

            // Remove the item from the slot
            equipment[slot] = null;
        }
    }

    private void SetEquipmentBlendShapes(ItemEquipment equipment, int weight)
    {
        foreach (EquipmentMeshRegion region in equipment.meshRegions)
        {
            targetMesh.SetBlendShapeWeight((int)region, weight);
        }
    }

    protected bool CanEquip(Item item, int slot)
    {
        // If it's not null 
        if (item != null)
        {
            // If the item is equipment
            if (item is ItemEquipment)
            {
                ItemEquipment newEquipment = item as ItemEquipment;

                if (// It's the correct slot
                    (int)newEquipment.equipmentSlot == slot &&
                    // The player is high enough level
                    playerCharacter.level >= newEquipment.minLevel)
                {
                    return true;
                }
            }
        }

        return false;
    }
}