﻿using UnityEngine;

[System.Serializable]
public class Inventory
{
    public Item[] items { get; private set; }

    public ItemBag bag { get; private set; }

    public int bagIndex { get; private set; }

    public Inventory(ItemBag newBag, int bagIndex)
    {
        SetBag(newBag);
        this.bagIndex = bagIndex;
    }

    public void SetBag(ItemBag newBag)
    {
        if(newBag == null)
        {
            bag = null;
        }
        else
        {
            bag = newBag;
            items = new Item[bag.size];
        }
    }

    public void SetBagIndex(int newBagIndex)
    {
        bagIndex = newBagIndex;
        if (items != null)
        {
            for (int i = 0; i < items.Length; i++)
            {
                if (items[i] != null)
                    items[i].inventoryBagIndex = bagIndex;
            }
        }
    }

    public void ChangeBag(ItemBag newBag)
    {
        // Create a copy of the items
        Item[] itemsCopy = items;
        
        // Change the bag and create a new item list
        SetBag(newBag);

        int n = 0;
        for (int i = 0; i < itemsCopy.Length; i++)
        {
            if(itemsCopy[i] != null)
            {
                items[n] = itemsCopy[i];
                items[n].inventoryItemIndex = n;
                n++;
            }
        }
    }

    public bool Add(Item newItem)
    {
        if (bag == null)
            return false;

        if (newItem == null)
            return true;

        Item currentItem;
        for (int i = 0; i < bag.size; i++)
        {
            currentItem = items[i];

            if (currentItem == null)
            {
                items[i] = newItem;

                newItem.inventoryBagIndex = bagIndex;
                newItem.inventoryItemIndex = i;
                newItem.inInventory = true;
                return true;
            }
            else if (currentItem.Equals(newItem))
            {
                if (newItem.count <= currentItem.stackSize - currentItem.count)
                {
                    items[i].count += newItem.count;

                    newItem.inventoryBagIndex = bagIndex;
                    newItem.inventoryItemIndex = i;
                    newItem.inInventory = false;
                    Object.Destroy(newItem);
                    return true;
                }
            }
        }
        return false;
    }
    public bool Add(Item newItem, int index)
    {
        if (index < 0 && index >= items.Length)
            return false;

        if (newItem == null)
            return true;

        Item currentItem = items[index];

        if (currentItem == null)
        {
            items[index] = newItem;

            newItem.inventoryBagIndex = bagIndex;
            newItem.inventoryItemIndex = index;
            newItem.inInventory = true;
            return true;
        }
        else if (currentItem.Equals(newItem))
        {
            if (newItem.count <= currentItem.stackSize - currentItem.count)
            {
                items[index].count += newItem.count;

                newItem.inventoryBagIndex = bagIndex;
                newItem.inventoryItemIndex = index;
                newItem.inInventory = false;
                Object.Destroy(newItem);
                return true;
            }
        }

        return false;
    }
    public void ForceAdd(Item newItem, int index)
    {
        if (index < 0 && index >= items.Length)
            return;

        items[index] = newItem;
        if(newItem != null)
        {
            newItem.inventoryBagIndex = bagIndex;
            newItem.inventoryItemIndex = index;
            newItem.inInventory = true;
        }   
    }

    public Item Remove(int index)
    {
        if (index < 0 && index >= items.Length)
            return null;

        Item item = items[index];
        items[index] = null;

        if (item != null)
            item.inInventory = false;

        return item;
    }

    public bool Remove(Item item)
    {
        if(item != null && item.inInventory && item == items[item.inventoryItemIndex] )
        {
            item.inInventory = false;
            items[item.inventoryItemIndex] = null;
            return true;
        }
        Debug.LogFormat("Failed to remove item: {0}, Bag:{1}, Item:{2}, In:{3}", item.name, item.inventoryBagIndex, item.inventoryItemIndex, item.inInventory);
        return false;
    }

    public bool Empty()
    {
        for (int i = 0; i < bag.size; i++)
        {
            if (items[i] != null)
                return false;
        }
        return true;
    }
    public int UsedSlots()
    {
        int count = 0;
        for (int i = 0; i < bag.size; i++)
        {
            if (items[i] != null)
                count++;
        }
        return count;
    }

    public Item GetItem(int index)
    {
        if (index < 0 && index >= items.Length)
            return null;

        return items[index];
    }
}
