﻿using UnityEngine;

public class PlayerInventory : MonoBehaviour
{
    public static PlayerInventory instance;

    public delegate void OnItemChanged();
    public OnItemChanged onItemChangedCallback;

    private void OnItemChangedCallback()
    {
        // The equipment has changed
        if (onItemChangedCallback != null)
            onItemChangedCallback.Invoke();
        else
            Debug.LogWarning("No subsicibers for onItemChangedCallback");
    }

    public int maxBags = 5;

    public Inventory[] inventories { get; private set; }

    public ItemBag[] bags;

    void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Debug.LogError("More than one PlayerInventory! " + gameObject.name);
            Destroy(this);
            return;
        }

        inventories = new Inventory[maxBags];
        for (int i = 0; i < maxBags; i++)
        {
            inventories[i] = new Inventory(bags[i], i);
        }
    }

    public void SetBag(ItemBag newBag, int index)
    {
        if (index < 0 && index >= bags.Length)
            return;

        bags[index] = newBag;
        inventories[index].SetBag(newBag);
    }

    public bool Add(Item newItem)
    {
        for (int i = 0; i < maxBags; i++)
        {
            if (HasBag(i))
            {
                if (inventories[i].Add(newItem))
                {
                    OnItemChangedCallback();
                    return true;
                }
            }
        }
        return false;
    }
    public bool Add(Item newItem, int bagIndex, int itemIndex)
    {
        if (HasBag(bagIndex))
        {
            if (inventories[bagIndex].Add(newItem, itemIndex))
            {
                OnItemChangedCallback();
                return true;
            }
        }
        return false;
    }

    public Item Remove(int bagIndex, int itemIndex)
    {
        if(HasBag(bagIndex))
        {
            Item item = inventories[bagIndex].Remove(itemIndex);
            if(item != null)
                OnItemChangedCallback();
            return item;
        }

        return null;
    }

    public bool Remove(Item item)
    {
        if(item != null && item.inInventory && HasBag(item.inventoryBagIndex))
        {
            if (inventories[item.inventoryBagIndex].Remove(item))
            {
                OnItemChangedCallback();
                return true;
            }
        }

        return false;
    }

    public Item GetItem(int bagIndex, int itemIndex)
    {
        if (HasBag(bagIndex))
            return inventories[bagIndex].GetItem(itemIndex);

        return null;
    }

    // Switching two items in the inventory
    public void SwitchItem (int bagIndexA, int itemIndexA, int bagIndexB, int itemIndexB)
    {
        if (!HasBag(bagIndexA) || !HasBag(bagIndexB))
            return;

        Item itemA = inventories[bagIndexA].Remove(itemIndexA);
        Item itemB = inventories[bagIndexB].Remove(itemIndexB);

        if (!inventories[bagIndexA].Add(itemB, itemIndexA) || !inventories[bagIndexB].Add(itemA, itemIndexB))
        {
            Debug.LogErrorFormat("Failed to switch items: ({0},{1}) , ({2},{3})", bagIndexA, itemIndexA, bagIndexB, itemIndexB);

            inventories[bagIndexA].ForceAdd(itemA, itemIndexA);
            inventories[bagIndexB].ForceAdd(itemB, itemIndexB);
        };

        OnItemChangedCallback();
    }

    // Swithcing two bags on the bag bar
    public void SwitchBag ( int indexA, int indexB)
    {
        Inventory inventoryA = inventories[indexA];
        inventories[indexA] = inventories[indexB];
        inventories[indexB] = inventoryA;

        ItemBag bagA = bags[indexA];
        bags[indexA] = bags[indexB];
        bags[indexB] = bagA;

        if(inventories[indexA] != null)
            inventories[indexA].SetBagIndex(indexA);
        if (inventories[indexB] != null)
            inventories[indexB].SetBagIndex(indexB);

        OnItemChangedCallback();
    }
    
    public void ChangeBag(ItemBag bag, int bagIndex, int itemBag, int itemIndex)
    {
        //Remove the new from the inventory
        inventories[itemBag].Remove(itemIndex);

        // Add the current bag to the inventory
        inventories[itemBag].Add(bags[bagIndex], itemIndex);

        // Move the contents
        inventories[bagIndex].ChangeBag(bag);

        // Equip the new bag
        bags[bagIndex] = bag;

        // The inventory has changed
        OnItemChangedCallback();
    }

    // Dragging an item from the inventory to the bag bar 
    public void SwitchToBag(int bagIndex, int itemBag, int itemIndex)
    {
        Item item = inventories[itemBag].items[itemIndex];

        // If the item is a bag
        if(item is ItemBag)
        {
            // There's currently no bag in the slot
            if (bags[bagIndex] == null)
            {
                // Equip the new bag
                SetBag(item as ItemBag, bagIndex);

                // Remove it from the inventory
                inventories[itemBag].Remove(itemIndex);

                // The inventory has changed
                OnItemChangedCallback();
            }
            // If there's a bag there
            else
            {
                // If the new bag can fit all the items in the current bag
                if ((item as ItemBag).size >= inventories[bagIndex].UsedSlots())
                {
                    ChangeBag(item as ItemBag, bagIndex, itemBag, itemIndex);
                }
                else
                {
                    Debug.Log("The new bag is too small");
                }
            }
        }
        // If it's any other item
        else
        {
            Debug.Log("Invalid item");
        }
    }

    // Draging a bag from the bag bar to the inventory
    public void SwitchFromBag(int bagIndex, int itemBag, int itemIndex)
    {
        Item item = inventories[itemBag].items[itemIndex];

        // If changing with another bag
        if (item is ItemBag)
        {
            // If the new bag can fit all the items in the current bag
            if ((item as ItemBag).size >= inventories[bagIndex].UsedSlots())
            {
                ChangeBag(item as ItemBag, bagIndex, itemBag, itemIndex);
            }
            else
            {
                Debug.Log("The new bag is too small");
            }
        }
        // If moving it to an empty slot
        else if (item == null)
        {
            // If trying to put a bag into it's own inventory
            if (bagIndex == itemBag)
            {
                Debug.Log("Can't put the bag into itself");
            }
            // The bag must be empty
            else if (inventories[bagIndex].Empty())
            {
                // Move the bag to the inventory
                inventories[itemBag].Add(bags[bagIndex], itemIndex);

                // Remove the bag from the bag bar
                SetBag(null, bagIndex);

                // The inventory has changed
                OnItemChangedCallback();
            }
            else
            {
                Debug.Log("Bag must be empty");
            }
        }
        // If trying to change it with any other item
        else
        {
            Debug.Log("Invalid item");
        }
    }
    public bool HasBag(int index)
    {
        if (index < 0 && index >= bags.Length)
            return false;

        if (bags[index] == null || inventories[index] == null || inventories[index].bag == null)
            return false;

        return true;
    }
}
