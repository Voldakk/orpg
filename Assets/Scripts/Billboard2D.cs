﻿using UnityEngine;

/// <summary>
/// Billboard that only rotates in one angle and looks flat from above
/// </summary>
public class Billboard2D : MonoBehaviour
{
    Camera cam;
    void Start()
    {
        cam = Camera.main;
    }
    void Update()
    {
        //made for a game where Y is up
        Vector3 rot = cam.transform.rotation.eulerAngles;
        rot.x = 0;
        rot.z = 0;
        transform.rotation = Quaternion.Euler(rot);
    }
}