Welcome to CTS - the Complete Terrain Shader!
================================

The CTS terrain shader is a profile driven shader, which means that you can generate as many profiles as you like, and apply them any time you want. 

CTS profiles keep their settings as you switch between design time and runtime. This allows you to configure your profile exactly the way you want in game, and retained the settings when you exit back to editor mode.

This set of profiles have been configured to use 2k textures which brings out the best in the textures. To use them first Add CTS to your terrain : Component -> CTS -> Add CTS To Terrain. Then click on a profile and select Apply Profile.

You can use these profiles as a starting point, however take a look at the performance optimisation readme in the top level CTS directory. There is a lot you can do to make them cheaper to run.