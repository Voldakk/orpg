Welcome to the CTS demos!
====================

To get the best out of the demo's do the following:

1. Set your lighting to Linear / Deferred : Component -> CTS -> Set Linear Deffered

2. Install the Unity Post Processing Stack from https://www.assetstore.unity3d.com/en/#!/content/83912

3. Press Play.

NOTE: You can do a lot with performance in CTS. Please check out CTS_Performance_Optimization in the top level CTS directory and experiment with your profiles.

Enjoy!!